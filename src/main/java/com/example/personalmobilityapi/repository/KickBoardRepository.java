package com.example.personalmobilityapi.repository;

import com.example.personalmobilityapi.entity.KickBoard;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KickBoardRepository extends JpaRepository<KickBoard, Long> {

}
