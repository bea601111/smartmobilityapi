package com.example.personalmobilityapi.repository;

import com.example.personalmobilityapi.entity.Member;
import com.example.personalmobilityapi.entity.RemainingAmount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RemainingAmountRepository extends JpaRepository<RemainingAmount, Long> {
    RemainingAmount findByMember(Member member);
}
