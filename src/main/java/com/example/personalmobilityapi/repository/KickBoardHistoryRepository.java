package com.example.personalmobilityapi.repository;

import com.example.personalmobilityapi.entity.KickBoardHistory;
import com.example.personalmobilityapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface KickBoardHistoryRepository extends JpaRepository<KickBoardHistory, Long> {
    List<KickBoardHistory> findAllByMemberAndIsComplete(Member member, boolean isComplete);
}
