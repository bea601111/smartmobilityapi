package com.example.personalmobilityapi.repository;

import com.example.personalmobilityapi.entity.AmountChargingHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AmountChargingHistoryRepository extends JpaRepository<AmountChargingHistory,Long> {
}
