package com.example.personalmobilityapi.service;

import com.example.personalmobilityapi.entity.KickBoard;
import com.example.personalmobilityapi.enums.KickBoardStatus;
import com.example.personalmobilityapi.exception.CMissingDataException;
import com.example.personalmobilityapi.model.KickBoardRequest;
import com.example.personalmobilityapi.repository.KickBoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class KickBoardService {
    private final KickBoardRepository kickBoardRepository;

    public KickBoard getKickBoardData(long id){
        return kickBoardRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public void setKickBoard(KickBoardRequest kickBoardRequest){
        KickBoard kickBoard = new KickBoard.Builder(kickBoardRequest).build();
        kickBoardRepository.save(kickBoard);
    }

    public void setStatus(KickBoard kickBoard, KickBoardStatus kickBoardStatus){
        kickBoard.setKickBoardStatus(kickBoardStatus);
        kickBoardRepository.save(kickBoard);
    }
}
