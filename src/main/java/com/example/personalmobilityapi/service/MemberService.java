package com.example.personalmobilityapi.service;

import com.example.personalmobilityapi.entity.Member;
import com.example.personalmobilityapi.exception.CDuplicateUsernameException;
import com.example.personalmobilityapi.exception.CMissingDataException;
import com.example.personalmobilityapi.exception.CNoUserNameException;
import com.example.personalmobilityapi.exception.CWrongPasswordException;
import com.example.personalmobilityapi.model.LoginRequest;
import com.example.personalmobilityapi.model.LoginResponse;
import com.example.personalmobilityapi.model.MemberRequest;
import com.example.personalmobilityapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getMemberData(long id){
        return memberRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public Member setMember(MemberRequest memberRequest){
        if (!isNewUserName(memberRequest.getUserName())) throw new CDuplicateUsernameException(); //만약 !isNewUserName(memberRequest.getUserName()이 true가 아니라면,
        // CDuplicateUsernameException()에게 던져라. -> 이름이 중복나면 쓰는 예외처리.
        Member member = new Member.Builder(memberRequest).build();
        return memberRepository.save(member);
    }

    //동일한 ID(아이디)가 있는지 확인하는 메소드
    private boolean isNewUserName(String username){
        long duplicateCount = memberRepository.countByUserName(username); //long 타입인 이유는 countBy(몇 개인지 센다)이기 때문이며, PK값의 갯수만큼
        //나올 수 있기 때문에 PK값의 타입과 동일하게 준다.

        return duplicateCount <= 0; //if(!duplicateCount <=0) return true
                                   // else return false 와 동일하다.
    }
    public LoginResponse doLogin(LoginRequest loginRequest){
        Member member = memberRepository.findByUserName(loginRequest.getUserName()).orElseThrow(CNoUserNameException::new); //repository에서 입력한 아이디를 입력해서 해당 데이터를 찾은 후,
        //Member 타입의 member에 넣어 놓는다. 만약 못 찾았다면, CNoUserNameException라는 직접 만든 예외처리로 던진다.
        // CNoUserNameException :: new는 새로운 빈 생성자를 생성하는 것과 같은 느낌인 것 같다.
        if (!member.getPassword().equals(loginRequest.getPassword())) throw new CWrongPasswordException(); //만약 입력받은 비밀번호와, member에 담긴 비밀번호 정보가 다를 경우
        //CWrongPasswordException라는 직접 만든 예외처리로 던진다.
        return new LoginResponse.Builder(member).build(); //위에 해당되지 않는다면, LoginResponse의 빌더에게 member를 주고, 가공해서 return시켜 달라고 한다.
    }
}
