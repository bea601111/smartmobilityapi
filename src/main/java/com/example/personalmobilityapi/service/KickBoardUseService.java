package com.example.personalmobilityapi.service;

import com.example.personalmobilityapi.entity.KickBoard;
import com.example.personalmobilityapi.entity.KickBoardHistory;
import com.example.personalmobilityapi.entity.Member;
import com.example.personalmobilityapi.exception.CMissingDataException;
import com.example.personalmobilityapi.model.KickBoardUseEndRequest;
import com.example.personalmobilityapi.model.KickBoardUseItem;
import com.example.personalmobilityapi.model.KickBoardUseStartRequest;
import com.example.personalmobilityapi.model.ListResult;
import com.example.personalmobilityapi.repository.KickBoardHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class KickBoardUseService {
    private final KickBoardHistoryRepository kickBoardHistoryRepository;

    public KickBoardHistory getHistoryList(long id){
        return kickBoardHistoryRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public void setUseStart(Member member, KickBoard kickBoard, KickBoardUseStartRequest kickBoardUseStartRequest){
        KickBoardHistory kickBoardHistory = new KickBoardHistory.Builder(member, kickBoard, kickBoardUseStartRequest).build();
        kickBoardHistoryRepository.save(kickBoardHistory);
    }

    public ListResult<KickBoardUseItem> getKickBoardHistory(Member member){
        List<KickBoardHistory> originList = kickBoardHistoryRepository.findAllByMemberAndIsComplete(member, false);

        List<KickBoardUseItem> result = new LinkedList<>();

        originList.forEach(e->{
            KickBoardUseItem getDatas = new KickBoardUseItem.Builder(e).build();

            result.add(getDatas);
        });
        return ListConvertService.settingResult(result);
    }
    public KickBoardHistory setUseEnd(long historyId, KickBoardUseEndRequest kickBoardUseEndRequest){
         KickBoardHistory kickBoardHistory = kickBoardHistoryRepository.findById(historyId).orElseThrow(CMissingDataException::new);

         kickBoardHistory.putKickBoardEndPos(kickBoardUseEndRequest);

         double defaultMoney = kickBoardHistory.getKickBoard().getPriceBasis().getBasePrice();
         double minuteMoney = kickBoardHistory.getKickBoard().getPriceBasis().getPerMinPrice();

         long totalSecond = ChronoUnit.SECONDS.between(kickBoardHistory.getDateStart(), kickBoardHistory.getDateEnd());

         double resultPrice = defaultMoney + (minuteMoney * ((double) totalSecond /60));

         kickBoardHistory.putResultPriceComplete(resultPrice);

         return kickBoardHistoryRepository.save(kickBoardHistory);
    }
}
