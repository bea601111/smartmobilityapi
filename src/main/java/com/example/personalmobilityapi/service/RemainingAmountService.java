package com.example.personalmobilityapi.service;

import com.example.personalmobilityapi.entity.Member;
import com.example.personalmobilityapi.entity.RemainingAmount;
import com.example.personalmobilityapi.repository.RemainingAmountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RemainingAmountService {
    private final RemainingAmountRepository remainingAmountRepository;

    //잔여 포인트를 0으로 설정하는 서비스
    public void setAmount(Member member){ //Member 타입의 인수를 받아서
        RemainingAmount remainingAmount = new RemainingAmount.Builder(member).build(); //해당 Member의 잔여포인트를 0으로 설정하는 RemainingAmount 엔티티의 빌더에게 넘겨준다.
        remainingAmountRepository.save(remainingAmount);
    }
    public RemainingAmount putAmountPrice(Member member, double minusPrice){
        RemainingAmount remainingAmount = remainingAmountRepository.findByMember(member);
        remainingAmount.putRemainPrice(minusPrice);
        return remainingAmountRepository.save(remainingAmount);
    }
}
