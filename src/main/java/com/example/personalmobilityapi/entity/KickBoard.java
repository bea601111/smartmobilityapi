package com.example.personalmobilityapi.entity;

import com.example.personalmobilityapi.enums.KickBoardStatus;
import com.example.personalmobilityapi.enums.PriceBasis;
import com.example.personalmobilityapi.interfaces.CommonModelBuilder;
import com.example.personalmobilityapi.model.KickBoardRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoard {

    //시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private KickBoardStatus kickBoardStatus;

    @Column(nullable = false, length = 20)
    private String modelName;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private PriceBasis priceBasis;

    @Column(nullable = false)
    private LocalDate dateBuy;

    @Column(nullable = false)
    private Double posX;

    @Column(nullable = false)
    private Double posY;

    @Column(columnDefinition = "TEXT")
    private String memo;

    @Column(nullable = false)
    private Boolean isUse;

    public void setKickBoardStatus(KickBoardStatus kickBoardStatus){
        this.kickBoardStatus = kickBoardStatus;
    }

    private KickBoard(Builder builder){
        this.kickBoardStatus = builder.kickBoardStatus;
        this.modelName = builder.modelName;
        this.priceBasis = builder.priceBasis;
        this.dateBuy = builder.dateBuy;
        this.posX = builder.posX;
        this.posY = builder.posY;
        this.isUse = builder.isUse;
    }

    public static class Builder implements CommonModelBuilder<KickBoard> {

        private final KickBoardStatus kickBoardStatus;
        private final String modelName;
        private final PriceBasis priceBasis;
        private final LocalDate dateBuy;
        private final Double posX;
        private final Double posY;
        private final Boolean isUse;

        public Builder(KickBoardRequest kickBoardRequest){
            this.kickBoardStatus = KickBoardStatus.READY;
            this.modelName = kickBoardRequest.getModelName();
            this.priceBasis = kickBoardRequest.getPriceBasis();
            this.dateBuy = kickBoardRequest.getDateBuy();
            this.posX = kickBoardRequest.getPosX();
            this.posY = kickBoardRequest.getPosY();
            this.isUse = false;
        }


        @Override
        public KickBoard build() {
            return new KickBoard(this);
        }
    }

}
