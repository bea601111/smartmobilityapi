package com.example.personalmobilityapi.entity;

import com.example.personalmobilityapi.interfaces.CommonModelBuilder;
import com.example.personalmobilityapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RemainingAmount {

    //시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId")
    private Member member;

    @Column(nullable = false )
    private Double remainPrice;

    @Column(nullable = false)
    private Double mileage;

    public void putRemainPrice(double minusPrice){
        this.remainPrice -= minusPrice;
    }

    private RemainingAmount(Builder builder){
        this.member = builder.member;
        this.remainPrice = builder.remainPrice;
        this.mileage = builder.mileage;
    }

    public static class Builder implements CommonModelBuilder<RemainingAmount> {

        private final Member member;
        private final Double remainPrice;

        private final Double mileage;

        public Builder(Member member){
            this.member = member;
            this.remainPrice = 0D;
            this.mileage = 0D;
        }

        @Override
        public RemainingAmount build() {
            return new RemainingAmount(this);
        }
    }
}
