package com.example.personalmobilityapi.entity;

import com.example.personalmobilityapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AmountChargingHistory {

    //시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId",nullable = false)
    private Member member;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private Double price;

    private AmountChargingHistory(Builder builder){
        this.member = builder.member;
        this.dateCreate = builder.dateCreate;
        this.price = builder.price;
    }

    public static class Builder implements CommonModelBuilder<AmountChargingHistory> {

        private final Member member;
        private final LocalDateTime dateCreate;
        private final Double price;

        public Builder(Member member, Double price){
            this.member = member;
            this.dateCreate = LocalDateTime.now();
            this.price = price;
        }

        @Override
        public AmountChargingHistory build() {
            return new AmountChargingHistory(this);
        }
    }
}
