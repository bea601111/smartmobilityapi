package com.example.personalmobilityapi.entity;

import com.example.personalmobilityapi.interfaces.CommonModelBuilder;
import com.example.personalmobilityapi.model.MemberRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member {

    //시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //아이디
    @Column(nullable = false,length = 20,unique = true)
    private String userName;

    //비밀번호
    @Column(nullable = false)
    private String password;

    //이름
    @Column(nullable = false, length = 20)
    private String memberName;

    //연락처
    @Column(nullable = false, length = 13)
    private String phoneNumber;

    //먼허증번호
    @Column(nullable = false, length = 15)
    private String licenseNumber;

    //생년월일
    @Column(nullable = false)
    private LocalDate birthDay;

    @Column(nullable = false)
    private LocalDate registDate;

    private Member(Builder builder){
        this.userName = builder.userName;
        this.password = builder.password;
        this.memberName = builder.memberName;
        this.phoneNumber = builder.phoneNumber;
        this.licenseNumber = builder.licenseNumber;
        this.birthDay = builder.birthDay;
        this.registDate = builder.registDate;
    }

    public static class Builder implements CommonModelBuilder<Member>{

        private final String userName;
        private final String password;
        private final String memberName;
        private final String phoneNumber;
        private final String licenseNumber;
        private final LocalDate birthDay;

        private final LocalDate registDate;

        public Builder(MemberRequest memberRequest){
            this.userName = memberRequest.getUserName();
            this.password = memberRequest.getPassword();
            this.memberName = memberRequest.getMemberName();
            this.phoneNumber = memberRequest.getPhoneNumber();
            this.licenseNumber = memberRequest.getLicenseNumber();
            this.birthDay = memberRequest.getBirthDay();
            this.registDate = LocalDate.now();
        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }

}
