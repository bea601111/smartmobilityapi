package com.example.personalmobilityapi.entity;

import com.example.personalmobilityapi.interfaces.CommonModelBuilder;
import com.example.personalmobilityapi.model.KickBoardUseEndRequest;
import com.example.personalmobilityapi.model.KickBoardUseStartRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoardHistory {

    //시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId",nullable = false)
    private Member member;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kickboardId",nullable = false)
    private KickBoard kickBoard;

    @Column(nullable = false)
    private Double startPosX;

    @Column(nullable = false)
    private Double startPosY;

    @Column(nullable = false)
    private LocalDateTime dateStart;

    private Double endPosX;

    private Double endPosY;

    private LocalDateTime dateEnd;

    @Column(nullable = false)
    private Double resultPrice;

    @Column(nullable = false)
    private Boolean isComplete;

    public void putResultPriceComplete(double resultPrice){
        this.resultPrice = resultPrice;
        this.isComplete = true;
    }

    public void putKickBoardEndPos(KickBoardUseEndRequest kickBoardUseEndRequest){
        this.endPosX = kickBoardUseEndRequest.getEndPosX();
        this.endPosY = kickBoardUseEndRequest.getEndPosY();
        this.dateEnd = LocalDateTime.now();
    }

    private KickBoardHistory(Builder builder){
        this.member = builder.member;
        this.kickBoard = builder.kickBoard;
        this.startPosX = builder.startPosX;
        this.startPosY = builder.startPosY;
        this.dateStart = builder.dateStart;
        this.resultPrice = builder.resultPrice;
        this.isComplete = builder.isComplete;
    }

    public static class Builder implements CommonModelBuilder<KickBoardHistory> {

        private final Member member;
        private final KickBoard kickBoard;
        private final Double startPosX;
        private final Double startPosY;
        private final LocalDateTime dateStart;
        private final Double resultPrice;
        private final Boolean isComplete;

        public Builder(Member member, KickBoard kickBoard, KickBoardUseStartRequest kickBoardUseStartRequest){
            this.member = member;
            this.kickBoard = kickBoard;
            this.dateStart = LocalDateTime.now();
            this.resultPrice = 0D;
            this.startPosX = kickBoardUseStartRequest.getStartPosX();
            this.startPosY = kickBoardUseStartRequest.getStartPosY();
            this.isComplete = false;
        }

        @Override
        public KickBoardHistory build() {
            return new KickBoardHistory(this);
        }
    }
}
