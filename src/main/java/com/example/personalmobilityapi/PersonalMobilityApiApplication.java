package com.example.personalmobilityapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonalMobilityApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersonalMobilityApiApplication.class, args);
	}

}
