package com.example.personalmobilityapi.model;

import com.example.personalmobilityapi.entity.KickBoard;
import com.example.personalmobilityapi.entity.KickBoardHistory;
import com.example.personalmobilityapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoardUseItem {
    private Long id;
    private KickBoard kickBoard;
    private Double startPosX;
    private Double startPosY;
    private LocalDateTime dateStart;


    private KickBoardUseItem(Builder builder){
        this.id = builder.id;
        this.kickBoard = builder.kickBoard;
        this.startPosX = builder.startPosX;
        this.startPosY = builder.startPosY;
        this.dateStart = builder.dateStart;
    }

    public static class Builder implements CommonModelBuilder<KickBoardUseItem> {

        private final Long id;
        private final KickBoard kickBoard;
        private final Double startPosX;
        private final Double startPosY;
        private final LocalDateTime dateStart;

        public Builder(KickBoardHistory kickBoardHistory){
            this.id = kickBoardHistory.getId();
            this.kickBoard = kickBoardHistory.getKickBoard();
            this.startPosX = kickBoardHistory.getStartPosX();
            this.startPosY = kickBoardHistory.getStartPosY();
            this.dateStart = kickBoardHistory.getDateStart();
        }

        @Override
        public KickBoardUseItem build() {
            return new KickBoardUseItem(this);
        }
    }
}
