package com.example.personalmobilityapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KickBoardUseEndRequest {

    private Double endPosX;

    private Double endPosY;


}
