package com.example.personalmobilityapi.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class KickBoardUseStartRequest {

    @NotNull
    private Double startPosX;

    @NotNull
    private Double startPosY;
}
