package com.example.personalmobilityapi.model;

import com.example.personalmobilityapi.entity.KickBoard;
import com.example.personalmobilityapi.enums.KickBoardStatus;
import com.example.personalmobilityapi.enums.PriceBasis;
import com.example.personalmobilityapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoardItem {

    private Long id;
    private KickBoardStatus kickBoardStatus;
    private String modelName;
    private PriceBasis priceBasis;
    private LocalDate dateBuy;
    private Double posX;
    private Double posY;
    private String memo;
    private Boolean isUse;

    private KickBoardItem(Builder builder){
        this.id = builder.id;
        this.kickBoardStatus = builder.kickBoardStatus;
        this.modelName = builder.modelName;
        this.priceBasis = builder.priceBasis;
        this.dateBuy = builder.dateBuy;
        this.posX = builder.posX;
        this.posY = builder.posY;
        this.memo = builder.memo;
        this.isUse = builder.isUse;
    }

    public static class Builder implements CommonModelBuilder<KickBoardItem> {

        private final Long id;
        private final KickBoardStatus kickBoardStatus;
        private final String modelName;
        private final PriceBasis priceBasis;
        private final LocalDate dateBuy;
        private final Double posX;
        private final Double posY;
        private final String memo;
        private final Boolean isUse;

        public Builder(KickBoard kickBoard){
            this.id = kickBoard.getId();
            this.kickBoardStatus = kickBoard.getKickBoardStatus();
            this.modelName = kickBoard.getModelName();
            this.priceBasis = kickBoard.getPriceBasis();
            this.dateBuy = kickBoard.getDateBuy();
            this.posX = kickBoard.getPosX();
            this.posY = kickBoard.getPosY();
            this.memo = kickBoard.getMemo();
            this.isUse = kickBoard.getIsUse();
        }

        @Override
        public KickBoardItem build() {
            return new KickBoardItem(this);
        }
    }

}
