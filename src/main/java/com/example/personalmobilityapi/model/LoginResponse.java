package com.example.personalmobilityapi.model;

import com.example.personalmobilityapi.entity.Member;
import com.example.personalmobilityapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LoginResponse { //Response를 사용한 이유는 정해진 한 종류의 값만 주기 위해서?
    //예를 들어, 사용자가 로그인을 하기 위해서 Request를 통해 값을 입력해서 로그인을 했으면, 그 로그인을 해서 나오는 값이 있을 것이고,
    //그 값 중에 사용자에게 뿌려줄 정보를 추려 넣어놓은 것이 Response?
    //Request는 사용자에게 받아오기 위한 그릇이고, Response는 그 받아온 정보를 재가공해서 담기 위한? 그릇?

    private Long id;

    private String memberName;

    private LoginResponse(Builder builder){
        this.id = builder.id;
        this.memberName = builder.memberName;
    }

    public static class Builder implements CommonModelBuilder<LoginResponse> {

        private final Long id;

        private final String memberName;

        public Builder(Member member){
            this.id = member.getId();
            this.memberName = member.getMemberName();
        }

        @Override
        public LoginResponse build() {
            return new LoginResponse(this);
        }
    }
}
