package com.example.personalmobilityapi.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MemberRequest {

    @NotNull
    @Length(min = 1,max = 20)
    private String userName;

    @NotNull
    private String password;

    @NotNull
    @Length(min = 1, max = 20)
    private String memberName;

    @NotNull
    @Length(min = 11,max = 13)
    private String phoneNumber;

    @NotNull
    @Length(min = 15,max = 15)
    private String licenseNumber;

    @NotNull
    private LocalDate birthDay;

}
