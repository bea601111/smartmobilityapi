package com.example.personalmobilityapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SingleResult<T> extends CommonResult {
    private T data; //얘는 ResponseService에 정의가 되어 있고,

    /*
        public static <T> SingleResult<T> getSingleResult(T data) {
        SingleResult<T> result = new SingleResult<>();
        result.setData(data);
        setSuccessResult(result);
        return result;
    }
    */
    //MemberController에서 이렇게 쓰였는데,
    /*

    public SingleResult<LoginResponse> doLogin(@RequestBody @Valid LoginRequest loginRequest){ //아이디와 패스워드를 받아서 (@RequestBody) 하나만
        return ResponseService.getSingleResult(memberService.doLogin(loginRequest));
    }

     */
    // SingleResult<LoginResponse> T의 자리에 LoginResponse가 들어간 걸로 보아, LoginResponse 타입의 데이터를 Return시켜야 할 것이고,
    // Response에 SingleResult의 함수에 getSingleResult가 정의 되어 있는데,
    // 안을 보면 빈 생성자를 하나 만들어주고, 그 타입의 result에 데이터를 넣는다.
    // 예를 들자면 위의 memberService.doLogin(loginRequest)가 getResult에 들어가면,
    // return new LoginResponse.Builder(member).build() 이 값이 data가 될 것이고, 그걸 저장한 result를 setSuccessResult에게 적용시킨다.

    // setSuccessResult의 코드는 밑에 나와있다.
    /*
        private static void setSuccessResult(CommonResult result) {
        result.setIsSuccess(true);
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());
    }*/
}

