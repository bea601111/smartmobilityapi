package com.example.personalmobilityapi.controller;

import com.example.personalmobilityapi.model.*;
import com.example.personalmobilityapi.service.KickBoardService;
import com.example.personalmobilityapi.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/kick-board-info")
public class KickBoardController {
    private final KickBoardService kickBoardService;

    @PostMapping("/data")
    public CommonResult setKickBoard(@RequestBody @Valid KickBoardRequest kickBoardRequest){
        kickBoardService.setKickBoard(kickBoardRequest);
        return ResponseService.getSuccessResult();
    }
}
