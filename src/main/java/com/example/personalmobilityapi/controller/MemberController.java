package com.example.personalmobilityapi.controller;

import com.example.personalmobilityapi.entity.Member;
import com.example.personalmobilityapi.model.*;
import com.example.personalmobilityapi.service.MemberService;
import com.example.personalmobilityapi.service.RemainingAmountService;
import com.example.personalmobilityapi.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;
    private final RemainingAmountService remainingAmountService;

    @PostMapping("/data")
    public CommonResult setMember(@RequestBody @Valid MemberRequest memberRequest){
        Member member = memberService.setMember(memberRequest);  //setMember에서 저장된 값을 return 시켜줬기 때문에 이 값을 Member라는 틀의 member 이름에 넣어준 후,
        remainingAmountService.setAmount(member); //setAmount에서 Member 타입의 인수를 필요로 했기 때문에 위의 member를 넣어준다.
        return ResponseService.getSuccessResult();
    }
    @PostMapping("/login")
    public SingleResult<LoginResponse> doLogin(@RequestBody @Valid LoginRequest loginRequest){ //아이디와 패스워드를 받아서 (@RequestBody) 하나만
        return ResponseService.getSingleResult(memberService.doLogin(loginRequest));
    }
}
