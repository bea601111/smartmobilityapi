package com.example.personalmobilityapi.controller;

import com.example.personalmobilityapi.entity.KickBoard;
import com.example.personalmobilityapi.entity.KickBoardHistory;
import com.example.personalmobilityapi.entity.Member;
import com.example.personalmobilityapi.enums.KickBoardStatus;
import com.example.personalmobilityapi.model.*;
import com.example.personalmobilityapi.service.*;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/kick-board-use")
public class KickBoardUseController {
    private final KickBoardUseService kickBoardUseService;
    private final MemberService memberService;
    private final KickBoardService kickBoardService;
    private final RemainingAmountService remainingAmountService;

    @PostMapping("/start/member-id/{memberId}/kick-board/{kickBoardId}")
    public CommonResult setUseStart(@PathVariable long memberId, @PathVariable long kickBoardId, @RequestBody @Valid KickBoardUseStartRequest kickBoardUseStartRequest){
        Member member = memberService.getMemberData(memberId);
        KickBoard kickBoard = kickBoardService.getKickBoardData(kickBoardId);
        kickBoardUseService.setUseStart(member, kickBoard, kickBoardUseStartRequest);

        //킥보드를 사용하기 시작했다는 의미니까 킥보드의 상태를 사용중으로 변경한다.
        kickBoardService.setStatus(kickBoard, KickBoardStatus.ING);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/ing/member-id/{memberId}")
    public ListResult<KickBoardUseItem> getKickBoardHistory(@PathVariable long memberId){
        Member member = memberService.getMemberData(memberId);

        return ResponseService.getListResult(kickBoardUseService.getKickBoardHistory(member),true);
    }

    @PutMapping("/end-data/history-id/{historyId}")
    public CommonResult setUseEnd(@PathVariable long historyId, @RequestBody @Valid KickBoardUseEndRequest kickBoardUseEndRequest){
         KickBoardHistory kickBoardHistory = kickBoardUseService.setUseEnd(historyId, kickBoardUseEndRequest);
         remainingAmountService.putAmountPrice(kickBoardHistory.getMember(),kickBoardHistory.getResultPrice());

         return ResponseService.getSuccessResult();
    }
}
