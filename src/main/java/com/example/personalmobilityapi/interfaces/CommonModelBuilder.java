package com.example.personalmobilityapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}