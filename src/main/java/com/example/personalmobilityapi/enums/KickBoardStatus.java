package com.example.personalmobilityapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum KickBoardStatus {
    READY("대기"),
    PAUSE("일시정지"),
    ING("사용중"),
    FIX("점검중");

    private final String name;
}
