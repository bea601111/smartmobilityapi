package com.example.personalmobilityapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum chargeMoney {

    THOUSAND,
    THREETHOUSAND,
    MILLION;

}
