package com.example.personalmobilityapi.exception;

public class CDuplicateUsernameException extends RuntimeException {
    public CDuplicateUsernameException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDuplicateUsernameException(String msg) {
        super(msg);
    }

    public CDuplicateUsernameException() {
        super();
    }
}
