package com.example.personalmobilityapi.exception;

public class CWrongPasswordException extends RuntimeException {
    public CWrongPasswordException(String msg, Throwable t) {
        super(msg, t);
    }

    public CWrongPasswordException(String msg) {
        super(msg);
    }

    public CWrongPasswordException() {
        super();
    }
}
